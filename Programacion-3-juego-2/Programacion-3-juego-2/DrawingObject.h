#ifndef DRAWINGOBJECT_H
#define DRAWINGOBJECT_H
#include <SFML/Graphics.hpp>

class DrawingObject
{
public:
	DrawingObject();
	virtual ~DrawingObject();
	enum Direction { Left, Right, Up, Down, None };
	void SetBoundingBox();
	bool SimpleCollision(DrawingObject *object2);
	int CheckBounds(int X, int Y);
	sf::Sprite sprite;
protected:
	void SetTexture(std::string SpriteNameWithFormat, int sizeX, int sizeY);
	Direction direction;
	sf::Texture texture;
	sf::FloatRect BoundingBox;
	int WindowHeight;
	int WindowWidth;
	int SpriteHeight;
	int SpriteWidth;
	const float MovDistance;
};
#endif //DRAWINGOBJECT_H
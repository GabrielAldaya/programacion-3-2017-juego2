#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>

#include "DrawingObject.h"

class Player : public DrawingObject
{
private :
	std::string Name;
	int Lives;
	int Points;
	bool InmuneState;
public:
	Player();
	Player(std::string _name, int _posX , int _posY);
	~Player() override;
	void MoveRight();
	void MoveLeft();
	void MoveUp();
	void MoveDown();
	void TakeDamage();
	int GetLives();
	bool CheckGameOver();
	void AddPoints(int points);
	int GetPoints();
	void SetInmuneState(bool _inmuneState);
	bool GetInmuneState();
};
#endif //PLAYER:H
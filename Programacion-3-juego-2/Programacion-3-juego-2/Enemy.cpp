#include "Enemy.h"

Enemy::Enemy()
{
	SetTexture("enemy.jpg", SpriteWidth,SpriteHeight);
	sprite.setPosition(0, 0);
}

Enemy::Enemy(int _posX, int _posY) {
	SetTexture("enemy.jpg", SpriteWidth, SpriteHeight);
	sprite.setPosition(_posX, _posY);
}

Enemy::~Enemy()
{
}

void Enemy::HorizontalMovement(bool Right)
{
	if (Right) {
		MoveRight();
	}
	else {
		MoveLeft();
	}
}

void Enemy::VerticalMovement(bool Up)
{
	if (Up) {
		MoveUp();
	}
	else {
		MoveDown();
	}
}

void Enemy::MoveRight()
{
	sprite.move(MovDistance, 0);
	direction = Right;
}
void Enemy::MoveLeft()
{
	sprite.move(-MovDistance, 0);
	direction = Left;
}
void Enemy::MoveUp()
{
	sprite.move(0, -MovDistance);
	direction = Up;
}
void Enemy::MoveDown()
{
	sprite.move(0, MovDistance);
	direction = Down;
}
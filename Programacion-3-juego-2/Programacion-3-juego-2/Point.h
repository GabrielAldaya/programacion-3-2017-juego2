#ifndef POINT_H
#define POINT_H
#include "DrawingObject.h"
class Point : public DrawingObject
{
private:
	int Value;
public:
	Point();
	Point(int X, int Y);
	~Point() override;
	void SetValue(int _value);
	int GetValue();
};
#endif //POINT_H
#include "DrawingObject.h"

DrawingObject::DrawingObject() : MovDistance(8), SpriteHeight(64), SpriteWidth(64)
{
}

DrawingObject::~DrawingObject()
{
}

void DrawingObject::SetTexture(std::string SpriteNameWithFormat, int sizeX, int sizeY) 
{
	texture.loadFromFile("../Assets/"+SpriteNameWithFormat, sf::IntRect(0, 0, sizeX, sizeY));
	sprite.setTexture(texture);
}

void DrawingObject::SetBoundingBox() {
	BoundingBox = sprite.getGlobalBounds();
}

bool DrawingObject::SimpleCollision(DrawingObject *object2) {

	if (BoundingBox.intersects(object2->BoundingBox))
	{
		return true;
	}
	else {
		return false;
	}
};

int DrawingObject::CheckBounds(int X, int Y) {
	//Horizontal
	if (sprite.getPosition().x + sprite.getLocalBounds().width + MovDistance > X) {
		return DrawingObject::Right;
	}
	else if (sprite.getPosition().x - MovDistance  < 0) {
		return DrawingObject::Left;
	}
	//vertical
	if (sprite.getPosition().y + sprite.getLocalBounds().height + MovDistance > Y) {
		return DrawingObject::Down;
	}
	else if (sprite.getPosition().y - MovDistance < 0) {
		return DrawingObject::Up;
	}
	return DrawingObject::None;
};

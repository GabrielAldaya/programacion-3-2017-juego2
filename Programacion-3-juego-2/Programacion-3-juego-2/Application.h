#ifndef APPLICATION_H
#define APPLICATION_H
#include "SfmlWindow.h"
#include "Scene.h"
#include "Game.h"
#include "Menu.h"
#include "Credits.h"
#include <windows.h>

class Application
{
private:
	int CurrentSceneName;
	Scene *CurrentScene;
	Scene *PrevScene;
	SfmlWindow *SceneWindow;
	enum Scenes { MenuScene, GameScene, CreditsScene };
public:
	Application();
	~Application();
	void RunApplication();
	void GoToMenuScene();
	void GoToGameScene();
	void GoToCreditsScene();
};
#endif //APPLICATION_H
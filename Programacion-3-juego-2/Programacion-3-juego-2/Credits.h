#ifndef CREDITS_H
#define CREDITS_H
#include "Scene.h"

class Credits : public Scene
{
private:
	void Start() override;
	void Update() override;
	sf::Text text;
	sf::Font font;
public:
	Credits();
	Credits(SfmlWindow *_window, Application *_app);
	~Credits() override;
};
#endif // CREDITS_H
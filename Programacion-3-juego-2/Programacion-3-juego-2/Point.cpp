#include "Point.h"

Point::Point()
{
	SetTexture("point.png", SpriteWidth,SpriteHeight);
	SetValue(1);
}

Point::Point(int X, int Y)
{
	SetTexture("point.png",SpriteWidth,SpriteHeight);
	SetValue(1);
	sprite.setPosition(X, Y);
}

Point::~Point()
{
}

void Point::SetValue(int _value) {
	Value = _value;
}

int Point::GetValue() {
	return Value;
}
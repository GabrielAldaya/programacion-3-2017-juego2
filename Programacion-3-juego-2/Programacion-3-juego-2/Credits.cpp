#include "Credits.h"
#include "Application.h"

Credits::Credits()
{
}

Credits::Credits(SfmlWindow *_window, Application *_app) : Scene(_window, _app)
{
	Start();
}

Credits::~Credits()
{
}

void Credits::Start() {
	std::cout << "Credits" << std::endl;
	//font
	if (!font.loadFromFile("../Assets/arial.ttf"))
	{
		std::cout << "Fail Load Font" << std::endl;
	}
	text.setFont(font);
	text.setCharacterSize(28); // in pixels, not points!
	text.setFillColor(sf::Color::White);
	text.setStyle(sf::Text::Regular);
	text.setPosition(SceneWindow->Width / 10, SceneWindow->Height / 6);

	text.setString("Made by Gabriel Aldaya\nSprites made with Paint\nMusic made with Fl Studio\nMade with SFML and RapidJson\n\nPress Spacebar to go back to the Menu");
}

void Credits::Update() {
	while (SceneWindow->window.pollEvent(SceneWindow->event))
	{
		switch (SceneWindow->event.type)
		{
		case sf::Event::KeyPressed:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
				App->GoToMenuScene();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				SceneWindow->window.close();
			}
			break;

		case sf::Event::Closed:
			SceneWindow->window.close();
			break;
		default:
			break;
		}
	}

	SceneWindow->clock.restart();
	SceneWindow->window.clear(sf::Color::Black);
	SceneWindow->window.draw(text);
	SceneWindow->window.display();
}
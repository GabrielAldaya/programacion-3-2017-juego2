#ifndef GAME_H
#define GAME_H

#include <SFML/Audio.hpp>
#include "Scene.h"
#include "DrawingObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Point.h"
#include <fstream>
#include <sstream>

class Game : public Scene
{
private:
	void Start() override;
	void Update() override;
	void Input();
	Player *player;
	Enemy *enemy;
	Enemy *enemy2;
	Point *point;
	Point *point2;
	sf::Text text;
	sf::Font font;
	std::ostringstream oss;
	sf::Vector2f v2;
	bool right;
	bool up;
	float timer;
	float inmuneTimer;
	sf::Music music;
	sf::Sound sound;
	sf::SoundBuffer buffer;
public:
	Game();
	Game(SfmlWindow *_window, Application *_app);
	~Game() override;
};
#endif //GAME_H

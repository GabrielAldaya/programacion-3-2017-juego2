#include "Application.h"

Application::Application()
{
#if defined(_DEBUG)
	std::cout << "Debug Console:" << std::endl;
#else
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	FreeConsole();
#endif
	SceneWindow = new SfmlWindow();
	GoToMenuScene();
	RunApplication();
}

Application::~Application()
{
}

void Application::RunApplication() {
	while (SceneWindow->window.isOpen())
	{
		if (PrevScene!=NULL) {
			delete PrevScene;
			PrevScene = NULL;
		}
		CurrentScene->RunScene();
	}
}

void Application::GoToMenuScene() {
	CurrentSceneName = MenuScene;
	PrevScene = CurrentScene;
	CurrentScene = new Menu(SceneWindow, this);
}
void Application::GoToGameScene() {
	CurrentSceneName = GameScene;
	PrevScene = CurrentScene;
	CurrentScene = new Game(SceneWindow, this);
} 
void Application::GoToCreditsScene() {
	CurrentSceneName = CreditsScene;
	PrevScene = CurrentScene;
	CurrentScene = new Credits(SceneWindow, this);
}
#include "SfmlWindow.h"

SfmlWindow::~SfmlWindow()
{
}

SfmlWindow::SfmlWindow() : Width(640), Height(480), window(sf::VideoMode(Width, Height), "SFML")
{
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);	
}
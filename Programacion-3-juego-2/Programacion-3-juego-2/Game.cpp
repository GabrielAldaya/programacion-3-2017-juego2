#include "Game.h"
#include "Application.h"

Game::Game()
{
}

Game::Game(SfmlWindow *_window, Application *_app) : Scene(_window,_app)
{
	Start();
}

Game::~Game()
{
}

void SaveHighscore(Player *player) {
	std::ifstream readFile;
	int value = 0;
	readFile.open("Highscore.txt");
	if (readFile.is_open())
	{
		std::string read;
		std::getline(readFile, read);
		value = std::stoi(read);
		readFile.close();
	}
	else {
		std::ofstream myFile;
		myFile.open("Highscore.txt");
		if (myFile.is_open()) {
			myFile << player->GetPoints() << std::endl;
			myFile.flush();
			myFile.close();
		}
	}
	if (player->GetPoints() > value) {
		std::ofstream myFile;
		myFile.open("Highscore.txt");
		if (myFile.is_open()) {
			myFile << player->GetPoints() << std::endl;
			myFile.flush();
			myFile.close();
		}
	}
}

void Game::Input() {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
		SaveHighscore(player);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{		
		if (player->CheckBounds(v2.x,v2.y) != DrawingObject::Left){
			player->MoveLeft();
		}
	}
	else 	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		if (player->CheckBounds(v2.x, v2.y) != DrawingObject::Right) {
			player->MoveRight();
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		if (player->CheckBounds(v2.x, v2.y) != DrawingObject::Up) {
			player->MoveUp();
		}
	}
	else 	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		if (player->CheckBounds(v2.x, v2.y) != DrawingObject::Down) {
			player->MoveDown();
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		App->GoToMenuScene();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		SceneWindow->window.close();
	}
}

void Game::Start() {	
	player = new Player("Pepe",365,255);
	enemy = new Enemy(64,64);
	enemy2 = new Enemy(192,128);
	point = new Point(240, 240);
	point2 = new Point(420, 360);
	v2 = sf::Vector2f(SceneWindow->Width, SceneWindow->Height);
	right = true;
	up = true;
	timer = 0;

	//font
	if (!font.loadFromFile("../Assets/arial.ttf"))
	{
		std::cout << "Fail Load Font" << std::endl;
	}
	text.setFont(font); 
	text.setCharacterSize(24); // in pixels, not points!
	text.setFillColor(sf::Color::Red);
	text.setStyle(sf::Text::Bold | sf::Text::Underlined);
	oss.clear();
	oss.str("");
	oss << "Lives: " << player->GetLives() << "\n Points: " << player->GetPoints();
	text.setString(oss.str());

	if (!buffer.loadFromFile("../Assets/sound.wav")) {
		std::cout << "fail load sound" << std::endl;
	}
	if (!music.openFromFile("../Assets/menu.ogg")) {
		std::cout << "fail load music" << std::endl;
	}

	sound.setBuffer(buffer);
	music.play();
	music.setLoop(true);	
}

void Game::Update() {
		
	sf::Time elapsed = SceneWindow->clock.restart();
	player->SetBoundingBox();
	enemy->SetBoundingBox();
	enemy2->SetBoundingBox();
	if (point != NULL) {
		point->SetBoundingBox();
	}
	if (point2 != NULL) {
		point2->SetBoundingBox();
	}

	while (SceneWindow->window.pollEvent(SceneWindow->event))
	{
		switch (SceneWindow->event.type)
		{
		case sf::Event::KeyPressed:
			Input();
			break;

		case sf::Event::Closed:
			SceneWindow->window.close();
			break;
		default:
			break;
		}
	}


	if (enemy->CheckBounds(v2.x, 0) == DrawingObject::Right) {
		right = false;
	}
	else if (enemy->CheckBounds(v2.x, 0) == DrawingObject::Left) {
		right = true;
	}

	if (enemy2->CheckBounds(v2.x, v2.y) == DrawingObject::Up) {
		up = false;
	}
	else if (enemy2->CheckBounds(v2.x, v2.y) == DrawingObject::Down) {
		up = true;
	}

	timer += elapsed.asSeconds();
	if (timer > 0.1) {
		enemy->HorizontalMovement(right);
		enemy2->VerticalMovement(up);
		timer = 0;
	}

	if (enemy->SimpleCollision(enemy2)) {
		if (up) {
			up = false;
		}
		else {
			up = true;
		}
	}
	if (!player->GetInmuneState()) {
		if (player->SimpleCollision(enemy) || player->SimpleCollision(enemy2)) {
			player->TakeDamage();
			oss.clear();
			oss.str("");
			oss << "Lives: " << player->GetLives() << "\n Points: " << player->GetPoints();
			text.setString(oss.str());
			player->SetInmuneState(true);
			player->sprite.setColor(sf::Color(255, 125, 125, 255));
		}
	}
	else {
		inmuneTimer += elapsed.asSeconds();
		if (inmuneTimer >= 0.1 && inmuneTimer < 0.2) {
			player->sprite.setColor(sf::Color(255, 255, 255, 125));
		}
		if (inmuneTimer >= 2) {
			player->SetInmuneState(false);
			player->sprite.setColor(sf::Color(255, 255, 255, 255));
			inmuneTimer = 0;
		}
	}

	if (point != NULL) {
		if (player->SimpleCollision(point)) {
			player->AddPoints(point->GetValue());
			oss.clear();
			oss.str("");
			oss << "Lives: " << player->GetLives() << "\n Points: " << player->GetPoints();						
			text.setString(oss.str());
			delete point;
			point = NULL;
			sound.play();
		}
	}

	if (point2 != NULL) {
		if (player->SimpleCollision(point2)) {
			player->AddPoints(point2->GetValue());
			oss.clear();
			oss.str("");
			oss << "Lives: " << player->GetLives() << "\n Points: " << player->GetPoints();
			text.setString(oss.str());
			delete point2;
			point2 = NULL;
			sound.play();
		}
	}

	if (player->CheckGameOver()) {
		SaveHighscore(player);
		App->GoToMenuScene();
	}

	SceneWindow->window.clear(sf::Color::White);
	SceneWindow->window.draw(player->sprite);
	SceneWindow->window.draw(enemy->sprite);
	SceneWindow->window.draw(enemy2->sprite);
	if (point != NULL) {
		SceneWindow->window.draw(point->sprite);
	}
	if (point2 != NULL) {
		SceneWindow->window.draw(point2->sprite);
	}
	SceneWindow->window.draw(text);
	SceneWindow->window.display();
}
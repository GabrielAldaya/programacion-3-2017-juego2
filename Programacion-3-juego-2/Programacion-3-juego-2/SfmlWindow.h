#ifndef SFMLWINDOW_H
#define SFMLWINDOW_H
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class SfmlWindow
{
public:
	SfmlWindow();
	~SfmlWindow();
	int Width ;
	int Height;
	sf::RenderWindow window;
	sf::Event event;
	sf::Clock clock;
};
#endif //SFMLWINDOW_H
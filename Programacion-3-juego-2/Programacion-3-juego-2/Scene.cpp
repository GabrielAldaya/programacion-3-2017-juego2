#include "Scene.h"
#include "Application.h"

Scene::Scene()
{
}

Scene::Scene(SfmlWindow *window, Application *_app)
{
	SceneWindow = window;
	App = _app;
}

Scene::~Scene()
{
}

void Scene::Start() 
{
}

void Scene::Update() 
{
}

void Scene::RunScene()
{
	Update();
}


#include "Menu.h"
#include "Application.h"

using namespace rapidjson;

Menu::Menu() 
{
}

Menu::Menu(SfmlWindow *_window, Application *_app) : Scene (_window,_app)
{
	Start();
}

Menu::~Menu()
{
}

void Menu::CheckWeather() {
	sf::Http http;
	http.setHost("http://query.yahooapis.com");

	sf::Http::Request request;
	request.setMethod(sf::Http::Request::Get);
	request.setUri("/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22buenosaires%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");   //"/page.html");
	request.setHttpVersion(1, 1); // HTTP 1.1

	sf::Http::Response response = http.sendRequest(request);

	if (response.getStatus() == sf::Http::Response::Ok)
	{
		Document document;
		std::string data = response.getBody();
		document.Parse(data.c_str());
		std::cout << "Buenos Aires Weather: ";
		std::cout << document["query"]["results"]["channel"]["item"]["condition"]["text"].GetString() << std::endl;
		Weather = "the weather in Buenos Aires is ";
		Weather += document["query"]["results"]["channel"]["item"]["condition"]["text"].GetString();
	}
	else
	{
		std::cout << "Web Request Failed" << std::endl;
		Weather = "here it would say the weather in Buenos Aires \nbut it doesn't";
	}
}

void Menu::ReadHighscore() {
	std::ifstream readFile;
	readFile.open("Highscore.txt");
	if (readFile.is_open())
	{
		std::string read;
		std::getline(readFile, read);
		std::cout << read << std::endl;
		Highscore = "Your Highscore is "+read;
		readFile.close();
	}
	else {
		Highscore = "If you have a Highscore i can't read it";
		std::cout << "Can't Open File" << std::endl;
	}
}

void Menu::Start() {
	//font
	if (!font.loadFromFile("../Assets/arial.ttf"))
	{
		std::cout << "Fail Load Font" << std::endl;
	}
	text.setFont(font);
	text.setCharacterSize(22); // in pixels, not points!
	text.setFillColor(sf::Color::Black);
	text.setStyle(sf::Text::Regular);
	text.setPosition(SceneWindow->Width / 12, SceneWindow->Height / 12);

	std::cout << "Menu" << std::endl;
	std::cout << "Highscore: ";
	ReadHighscore();
	CheckWeather();
	text.setString("Sup?\nPress the Spacebar to Play\nPress C to see the Credits\nPress ESC to Exit\n\n Also " + Highscore + "\n And " + Weather);
}

void Menu::Update() {

	while (SceneWindow->window.pollEvent(SceneWindow->event))
	{
		switch (SceneWindow->event.type)
		{
		case sf::Event::KeyPressed:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
				App->GoToGameScene();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
				App->GoToCreditsScene();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				SceneWindow->window.close();
			}
			break;

		case sf::Event::Closed:
			SceneWindow->window.close();
			break;
		default:
			break;
		}
	}

	SceneWindow->clock.restart();
	SceneWindow->window.clear(sf::Color::White);
	SceneWindow->window.draw(text);
	SceneWindow->window.display();
}
#include "Player.h"

Player::Player() : Name("default"), Lives (3), Points(0), InmuneState (false){
	SetTexture("player.jpg", SpriteWidth, SpriteHeight);
	direction = Right;
	sprite.setPosition(0, 0);
}

Player::Player(std::string _name , int _posX , int _posY) : Name(_name), Lives(3), Points(0), InmuneState(false)
{
	SetTexture("player.jpg", SpriteWidth, SpriteHeight);
	direction = Right;
	sprite.setPosition(_posX, _posY);
}

Player::~Player() 
{
}

void Player::MoveRight()
{
	sprite.move(MovDistance, 0);
	direction = Right;
}
void Player::MoveLeft()
{
	sprite.move(-MovDistance, 0);
	direction = Left;
}
void Player::MoveUp()
{
	sprite.move(0, -MovDistance);
	direction = Up;	
}
void Player::MoveDown()
{
	sprite.move(0, MovDistance);
	direction = Down;
}

void Player::TakeDamage()
{
	Lives--;
	std::cout << "Lives: ";
	std::cout << Lives << std::endl;
}

int Player::GetLives() {
	return Lives;
}

bool Player::CheckGameOver()
{
	if (Lives <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Player::AddPoints(int points)
{
	Points += points;
	std::cout << "Points: ";
	std::cout << Points << std::endl;
}

int Player::GetPoints() {
	return Points;
}

void Player::SetInmuneState(bool _inmuneState) {
	InmuneState = _inmuneState;
}

bool Player::GetInmuneState() {
	return InmuneState;
}
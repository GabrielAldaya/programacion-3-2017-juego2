#ifndef ENEMY_H
#define ENEMY_H
#include "DrawingObject.h"

class Enemy : public DrawingObject
{
private:
	void MoveRight();
	void MoveLeft();
	void MoveUp();
	void MoveDown();
public:
	Enemy();
	Enemy(int _posX, int _posY);
	~Enemy() override;	
	void HorizontalMovement(bool Right);
	void VerticalMovement(bool Up);
};
#endif //ENEMY_H
#ifndef SCENE_H
#define SCENE_H

#include "SfmlWindow.h"

class Application;

class Scene
{
protected:
	Application *App;
	SfmlWindow *SceneWindow;
	virtual void Start();
	virtual void Update();
public:
	Scene();
	Scene(SfmlWindow *window, Application *_app);
	virtual ~Scene();
	void RunScene();
};
#endif //SCENE_H
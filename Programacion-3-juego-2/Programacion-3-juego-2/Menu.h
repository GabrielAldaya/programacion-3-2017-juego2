#ifndef MENU_H
#define MENU_H
#include "Scene.h"
#include "rapidjson\document.h"
#include <SFML/Network.hpp>

class Menu : public Scene
{
private:
	void Start() override;
	void Update() override;
	void ReadHighscore();
	void CheckWeather();
	sf::Text text;
	sf::Font font;
	std::string Weather;
	std::string Highscore;
public:
	Menu();
	Menu(SfmlWindow *_window, Application *_app);
	~Menu() override;
};
#endif //MENU_H